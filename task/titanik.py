import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Prefix'] = df['Name'].apply(lambda name: name.split(',')[1].split('.')[0].strip() +  '.')
    #number of missing values
    miss1 = df[df['Prefix'] == 'Miss.']['Age'].isna().sum()
    mr1 = df[df['Prefix'] == 'Mr.']['Age'].isna().sum()
    mrs1 = df[df['Prefix'] == 'Mrs.']['Age'].isna().sum()
    
    #median
    miss2 = df[df['Prefix'] == 'Miss.']['Age'].median()
    mr2 = df[df['Prefix'] == 'Mr.']['Age'].median()
    mrs2 = df[df['Prefix'] == 'Mrs.']['Age'].median()
    
    values = [('Mr.', mr1, round(mr2)), ('Mrs.', mrs1, round(mrs2)), ('Miss.', miss1, round(miss2))]
    return values
